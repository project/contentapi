<?php

/**
 * Implementation of hook_menu().
 */
function contentapi_menu() {
  $items = array();

  $items['admin/settings/contentapi'] = array(
    'title' => t('Content API Settings'),
    'description' => t('Administer node resource fields.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('contentapi_settings'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );

  foreach (node_get_types() as $type) {
	$items['admin/settings/contentapi/'. $type->type] = array(
	  'title' => $type->name,
	  'page callback' => 'drupal_get_form',
      'page arguments' => array('contentapi_nodetype_display_form', $type->type),
	  'access arguments' => array('administer site configuration'),
      'type' => MENU_LOCAL_TASK,
	);
  }
	
  return $items;
}

function contentapi_nodetype_display_form(&$form_state, $type) {
  $content_type = content_types($type);
  $form = array();

  $form['type'] = array(
    '#type' => 'hidden',
    '#value' => $type,
  );

  $hidden_fields = unserialize(variable_get("contentapi_$type", ''));

  $form['fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fields'),
  );

  foreach ($content_type['fields'] as $field => $info) {
	$default = (is_array($hidden_fields) && in_array($field, $hidden_fields));
    $form['fields'][$field] = array(
	  '#title' => $info['widget']['label'],
      '#type' => 'checkbox',
	  '#default_value' => !$default
	);
  }

  $form['submit'] = array(
    '#type' => 'submit',
	'#value' => 'Submit'
  );

  return $form;
}

function contentapi_nodetype_display_form_submit($form, &$form_state) {
  $hidden_fields = array();
  $name = $form_state['values']['type'];

  foreach ($form_state['values'] as $key => $value) {
    if (substr($key, 0, 6) == 'field_' && $value === 0) {
      $hidden_fields[] = $key;
	}
  }

  variable_set("contentapi_$name", serialize($hidden_fields));
}

function contentapi_node_type($op, $info) {
  if ($op == 'insert') {
    $name = $info->type;
    variable_set("contentapi_$name", serialize(array()));
  }

  if ($op == 'delete') {
    $name = $info->type;
	variable_del("contentapi_$name");
  }
}

/**
 * Drupal get_form function for the admin settings menu for the node resource.
 */
function contentapi_settings() {
  $form = array();

  $hidden_fields = unserialize(variable_get("contentapi_node_hidden_fields", ''));
  $node_fields = array('created' => 'created',
	  				   'changed' => 'changed',
	  				   'title'   => 'title',
	  				   'webUrl'  => 'webUrl',
	                   'nid'     => 'nid',
	                   'type'    => 'type',
	                   'body'    => 'body',
	                   'vid'     => 'vid',
                       'taxonomy'=> 'taxonomy',
  				 );
  $defaults = array();
	
  foreach ($node_fields as $field) {
    $defaults[$field] = $field;
	if (is_array($hidden_fields) && in_array($field, $hidden_fields)) {
	  unset($defaults[$field]);
	}
  }

  $test = 'test';

  $form['contentapi_node_hidden_fields'] = array(
    '#type' => 'checkboxes',
	'#options' => $node_fields,
    '#default_value' => $defaults,
    '#title' => 'Node fields',
	'#description' => 'Only checked node fields will display in the api'
  );

  $form['contentapi_limit'] = array(
    '#type' => 'textfield',
	'#default_value' => variable_get('contentapi_limit', 10),
	'#title' => 'Node return limit',
	'#description' => 'Maximum number of nodes to return per api call'
  );

  $form['contentapi_sort_field'] = array(
	'#type' => 'textfield',
	'#default_value' => variable_get('contentapi_sort_field', 'created'),
	'#title' => 'Default sort field'
  );

  $form['contentapi_sort_order'] = array(
    '#type' => 'select',
	'#options' => array('ASC' => 'Ascending', 'DESC' => 'Descending'),
	'#title' => 'Default sort order',
    '#default_value' => variable_get('contentapi_sort_order', 'DESC')
  );

  $form['contentapi_show_count'] = array(
    '#type' => 'checkbox',
	'#title' => 'Show count item as first result',
	'#description' => 'Check to show an item that includes count, pages, itemsPerPage and currentPage',
	'#default_value' => variable_get('contentapi_show_count', TRUE)
  );

  $options = array_map('check_plain', node_get_types('names'));
  $types = variable_get('contentapi_content_types', '');
  $checked_options = unserialize($types);

  $form['contentapi_content_types'] = array(
    '#type' => 'checkboxes',
    '#options' => $options,
    '#title' => 'Node Types to publish in API',
    '#description' => 'Select the node types to publish in the api',
    '#default_value' => $checked_options
  );

  return system_settings_form($form);
}


function contentapi_settings_validate($form, &$form_state) {
  $types = $form_state['values']['contentapi_content_types'];
  $hidden_fields = $form_state['values']['contentapi_node_hidden_fields'];

  if ($types) {
    $form_state['values']['contentapi_content_types'] = serialize($types);
  }

  if ($hidden_fields) {
    $hide_fields = array();
	foreach ($hidden_fields as $field => $show) {
      if ($show === 0) $hide_fields[] = $field;
	}
    $form_state['values']['contentapi_node_hidden_fields'] = serialize($hide_fields);
  }

  // TODO: add more validation
}

/**
 * Implementation of hook_services_resources()
 * @return array
 */
function contentapi_services_resources() {
  $content_resource = array(
    'content' => array(
      'retrieve' => array(
        'callback' => '_contentapi_retrieve',
        'args' => array(
          array(
            'name' => 'nid',
            'optional' => FALSE,
            'source' => array('path' => 0),
            'type' => 'int',
            'description' => 'The nid of the node to get',
          ),
        ),
        'access callback' => '_contentapi_access',
        'access arguments' => array('view'),
        'access arguments append' => TRUE,
      ),
      'index' => array(
        'callback' => '_contentapi_index',
        'access arguments' => array('access content'),
      ),
    ),
  );

  return $content_resource;
}

/**
 * Determine whether the current user can access a node resource.
 *
 * @param $op
 *   One of view, update, create, delete per node_access().
 * @param $args
 *   Resource arguments passed through from the original request.
 * @return bool
 *
 * @see node_access()
 */
function _contentapi_access($op = 'view', $args = array()) {
  // Make sure we have an object or this all fails, some servers can
  // mess up the types.
  if (is_array($args[0])) {
    $args[0] = (object) $args[0];
  }
  elseif (!is_array($args[0]) && !is_object($args[0])) {  //This is to determine if it is just a string
    $args[0] = (object)array('nid' => $args[0]);
  }

  if ($op != 'create' && !empty($args)) {
    $node = node_load($args[0]->nid);
  }
  elseif ($op == 'create') {
    $node = $args[0]->type;
    return node_access($op, $node);
  }
  if (isset($node->nid) && $node->nid) {
    return node_access($op, $node);
  }
  else {
    return services_error('Node id: '. $args[0]->nid .' could not be found', 404);
  }
}

function contentapi_load_blacklisted_fields() {
  $blacklist = array();

  foreach(node_get_types() as $type) {
	$type_name = $type->type;
    $blacklist[$type_name] = unserialize(variable_get("contentapi_$type_name", ''));
  }

  return $blacklist;
}

/**
 * Returns the results of a node_load() for the specified node.
 *
 * This returned node may optionally take content_permissions settings into
 * account, based on a configuration setting.
 *
 * @param $nid
 *   NID of the node we want to return.
 * @param $fields
 *   boolean value to toggle showing all fields related to a node, default false
 * @return
 *   Node object or FALSE if not found.
 *
 */
function _contentapi_retrieve($nid, $fields = TRUE) {
  global $user;
  static $fields_blacklist;
  static $hidden_fields;

  if (!is_array($fields_blacklist)) {
    $fields_blacklist = contentapi_load_blacklisted_fields();
  }

  if (!is_array($hidden_fields)) {
    $hide_fields = variable_get('contentapi_node_hidden_fields', '');
    $hidden_fields = ($hide_fields) ? unserialize($hide_fields) : array();
  }

  $raw_node = node_load($nid);

  if ($raw_node) {
    // Create a new node object with only the desired fields
    // We are creating a whitelist of node fields that can be displayed to users
    $node = new stdClass;
    if (!in_array('nid', $hidden_fields)) $node->nid = $raw_node->nid;
    if (!in_array('title', $hidden_fields)) $node->title = $raw_node->title;
    if (!in_array('created', $hidden_fields)) $node->created = date('Y-m-d', $raw_node->created);
    if (!in_array('changed', $hidden_fields)) $node->changed = date('Y-m-d', $raw_node->changed);
    if (!in_array('type', $hidden_fields)) $node->type = $raw_node->type;
    if (!in_array('webUrl', $hidden_fields)) $node->webUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $raw_node->path;

    if ($fields) {
      if (!in_array('taxonomy', $hidden_fields)) $node->taxonomy = $raw_node->taxonomy;
      if (!in_array('body', $hidden_fields)) $node->body = $raw_node->body;
      if (!in_array('vid', $hidden_fields)) $node->vid = $raw_node->vid;

      // Load cck fields into the node object
      $cck_fields = content_storage('load', $raw_node);

      foreach ($cck_fields as $field => $value) {
        $node->$field = $value;
      }

	  $use_permissions = module_exists('content') && variable_get('services_use_content_permissions', TRUE);

      // Apply field level content permissions by removing any fields the user doesn't have access to either
	  // through permissions or through the contentapi admin settings
      // Some code from Services 3.x module node resource
      $fields = content_fields(NULL, $raw_node->type);
      foreach ($fields as $field_name => $field_info) {
        if (isset($node->$field_name)) {
		  if (is_array($fields_blacklist[$raw_node->type]) && in_array($field_name, $fields_blacklist[$node->type])) {
			unset($node->$field_name);
		  }
		  elseif ($use_permissions) {
            $access = module_invoke_all('field_access', 'view', $field_info, $user, $raw_node);
            if (in_array(FALSE, $access)) {
              unset($node->$field_name);
            }
          }
		}
      }
    }

    $node->uri = services_resource_uri(array('content', $node->nid));
    return $node;
  }
  return services_error('Node nid '. $nid .' not found', 404);
}

/**
 * Return an array of node objects based on input query parameters
 *
 * @return
 *   An array of node objects.
 */
function _contentapi_index() {

  // Match all CCK query parameters
  // CCK Fields are dynamic, so we use the cck api to get all the fields and match against any input
  // field parameters. We can then get a list of nids that match the corresponding cck fields.

  // Build a list of nids that meet the conditions for input filters
  $nids = array();
  $params = array();

  // All CCK field queries must start with 'field_'
  foreach ($_GET as $key => $value) {
    if (substr($key, 0, 6) == 'field_') {
      $params[$key] = $value;
    }
  }
  // If cck query params were passed, get a list of nids that meet the query parameters from our helper function
  if (!empty($params)) $nids = _contentapi_cck_query($params);

  // Join any nids into our base node query
  $cck_nids = ($nids) ? ' AND n.nid in (' . implode(',', $nids) . ')' : '';

  // Now that we've collected a series of nids, we'll perform a base query for node fields, taxonomy and include
  // pagination, limit and sort order using only nids we've verified meet the query conditions
    
  $params = array();
  $args = array();
  // Set filters for fields in the node table
  if ($_GET['title']) {
    $params['title'] = check_plain($_GET['title']);
  }
  if ($_GET['created_before']) {
    $params['created_before'] = check_plain($_GET['created_before']);
  }
  if ($_GET['created_after']) {
    $params['created_after'] = check_plain($_GET['created_after']);
  }
  if ($_GET['updated_before']) {
    $params['updated_before'] = check_plain($_GET['updated_before']);
  }
  if ($_GET['updated_after']) {
    $params['updated_after'] = check_plain($_GET['updated_after']);
  }
  if ($_GET['type'] && !is_array($_GET['type'])) {
    $params['type'] = check_plain($_GET['type']);
  }
  $params['status'] = 1; // Hard coding in status value, api is for external consumption so all nodes should be published
  $cond = array();
  // Create db query conditions for each parameter
  foreach ($params as $key => $value) {
    // Properly format date parameters and search operators
    if (substr($key, -7, 7) == '_before') {
      $cond[] = 'n.' . db_escape_table(substr($key, 0, -7)) .' <= "%s"';
      $args[] = strtotime($value);
    }
    elseif (substr($key, -6, 6) == '_after') {
      $cond[] = 'n.' . db_escape_table(substr($key, 0, -6)) .' >= "%s"';
      $args[] = strtotime($value);
    }
    else {
      $cond[] = 'n.'. db_escape_table($key) ." = '%s'";
      $args[] = $value;
    }
  }
  $cond = (!empty($cond) ? implode(' AND ', $cond) : '' );

  // If types is given as plural, it should be an array so we filter by type IN (types) instead of type = type
  $types = array();
  if ($_GET['types'] && is_array($_GET['types'])) {
    foreach ($_GET['types'] as $type) {
      $types[] = "'" . check_plain($type) . "'";
    }
  }
  if (!empty($types)) {
    $type_sql = 'AND n.type IN ('. implode(',', $types) .')';
  }
  else {
    $type_sql = '';
  }

  // Now check type settings and only include white-listed node types
  $access_types = unserialize(variable_get('contentapi_content_types', ''));
  if ($access_types) {
    $checked_types = array();
    foreach ($access_types as $type_name) {
      if (!is_int($type_name))
        $checked_types[] = '"'. $type_name .'"';
      }
      $type_access_sql = 'AND n.type IN(' . implode(',', $checked_types) . ')';
    }
    else {
      $type_access_sql = '';
    }

  $terms = array();
  // Taxonomy queries
  if ($_GET['terms'] && is_array($_GET['terms'])) {
    foreach ($_GET['terms'] as $term) {
      $terms[] = (integer)$term;
    }
  }
  $term_sql = (!empty($terms)) ? ' t.tid IN ('. implode(',', $terms) .')' : '';

  // Initialize search query if a search string is given
  // Searches titles by an input string
  if ($_GET['search_string']) {
    $search = check_plain($_GET['search_string']);
    $search_query = 'AND MATCH(n.title) AGAINST(\''. $search .'\' IN BOOLEAN MODE)';
  }
  $search_sql = ($search_query) ? $search_query : '';

  // Change sort field
  if ($_GET['sort']) {
    $sort = check_plain($_GET['sort']);
  }
  else {
    // Default sort field
	$sort = variable_get('contentapi_sort_field', 'created');
  }

  $sort_order = ($_GET['sort_order']) ? check_plain($_GET['sort_order']) : variable_get('contentapi_sort_order', 'DESC');
  $sort_sql = 'n.' . $sort . ' ' . $sort_order;

  // Number of items to return, default is 10
  $limit = ($_GET['limit']) ? (integer)$_GET['limit'] : 10;

  // Check limit cap, users must use pagination to get more results if cap is exceeded
  $limit_cap = variable_get('contentapi_limit', 10);
  if ($limit > $limit_cap) $limit = $limit_cap;

  // Page to return, default is 0
  $page = ($_GET['page']) ? (integer)$_GET['page'] : 0;

  // Query for node ids
  $sql = "SELECT n.nid FROM {node} n LEFT JOIN {term_node} t ON n.vid = t.vid
  WHERE $cond $type_sql $type_access_sql $cck_nids $term_sql $search_sql
  ORDER BY $sort_sql";

  $sql = db_rewrite_sql($sql);

  // Put together array of matching nodes to return.
  $results = array();
  $result = db_query_range($sql, $args, $page * $limit, $limit);
  $fields = ($_GET['fields'] && $_GET['fields'] == 'all');	

  $show_count = variable_get('contentapi_show_count', TRUE);

  if ($show_count) {
    // Calculate the number of matching results for pagination
    $count_sql = 'SELECT count(nid) AS count FROM(' . $sql . ') AS nodes';
    $count = db_fetch_object(db_query($count_sql, $args))->count;
    $count_array = array('count' => $count);
    $count_array['pages'] = (integer) ceil($count / $limit) - 1;
    if ($count_array['pages'] == -1) $count_array['pages'] = 0;
    $count_array['itemsPerPage'] = $limit;
    $count_array['currentPage'] = $page;

    $results[] = $count_array;
  }
	

  while ($node = db_fetch_object($result)) {
    $results[] = _contentapi_retrieve($node->nid, $fields);
  }

  return $results;
}

/**
 * @param $param - array of key->value pairs, where key is cck field and value is the query value for that field
 * @return array of nids, or FALSE if nothing meets conditions
 */
function _contentapi_cck_query($param = NULL) {

  if (!is_array($param)) {
    return FALSE;
  }
  // Initialize arrays
  $arguments = array();
  $cck_tables = array();
  $cck_cond = array();
  $cond = array();
  $i = 1;

  // Loop through each input key->value and add query conditions to match the parameters
  foreach ($param as $key => $value) {
    // Otherwise it's a cck field
    $db_info = content_database_info(content_fields($key));
    // If the field is from another table, increment sequence for table aliases
    if (!isset($cck_tables[$db_info['table']])) {
      $cck_tables[$db_info['table']] = 'cck'. $i;
    }
    $i++;
    if ($db_info['columns']['nid']) {
      // For nodereference fields
      $cck_cond[] = $cck_tables[$db_info['table']] .'.'. db_escape_table($db_info['columns']['nid']['column']) ." = '%s'";
    }
    elseif ($db_info['columns']['value']) {
      // Regular cck fields
      $cck_cond[] = $cck_tables[$db_info['table']] .'.'. db_escape_table($db_info['columns']['value']['column']) ." = '%s'";
    }
    elseif ($db_info['columns'][$key]['column']) {
      // For fields with specified values
      $cck_cond[] = $cck_tables[$db_info['table']] .'.'. db_escape_table($db_info['columns'][$key]['column']) ." = '%s'";
    }
    $arguments[] = $value;
  }
  // Piece together the different tables into a join query
  if (!empty($cck_cond)) {
    $cond = implode(' AND ', $cck_cond);
    $cck_join = '';
    foreach ($cck_tables as $table => $alias) {
      $cck_join .= ' INNER JOIN {'. $table .'} '. $alias .' ON '. $alias .'.nid = n.nid';
    }
  }
  // Create and run the query
  $sql = "SELECT n.nid FROM {node} n ". $cck_join ." WHERE ". $cond;
  $result = db_query(db_rewrite_sql($sql), $param);
  $nids = array();
  while ($nid = db_result($result)) {
    $nids[$nid] = $nid;
  }
  // Return nids array, or FALSE if none exist matching the conditions
  return (empty($nids) ? FALSE : $nids);
}